package implementation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import implementation.attributes.ProductType;
import implementation.hierarchy.Product;

public class AttributeFilter {

	public static List<Product> filterByProductType(List<Product> products, final ProductType type) {
		List<Product> filtered = new ArrayList<Product>();

		Predicate<Product> predicate = new Predicate<Product>() {
			@Override
			public boolean apply(Product product) {
				return product.getProductType().equals(type);
			}
		};

		Collection<Product> result = Collections2.filter(products, predicate);
		filtered.addAll(result);

		return filtered;
	}

	public static List<Product> filterByIssueDate(List<Product> products, String date) {
		List<Product> filteredProducts = new ArrayList<Product>();

		try {
			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			final Date convertedDate = formatter.parse(date);

			Predicate<Product> predicate = new Predicate<Product>() {
				@Override
				public boolean apply(Product product) {
					return product.getIssueDate().after(convertedDate);
				}
			};

			Collection<Product> result = Collections2.filter(products, predicate);
			filteredProducts.addAll(result);

		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return filteredProducts;
	}

	public static List<Product> filterOutProductType(List<Product> products, final ProductType type) {
		List<Product> filteredProducts = new ArrayList<Product>();

		Predicate<Product> predicate = new Predicate<Product>() {
			@Override
			public boolean apply(Product product) {
				return !(product.getProductType().equals(type));
			}
		};

		Collection<Product> result = Collections2.filter(products, predicate);
		filteredProducts.addAll(result);

		return filteredProducts;
	}

	public static List<Product> filterOutAfterIssueDate(List<Product> products, String date) {
		List<Product> filteredProducts = new ArrayList<Product>();

		try {
			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			final Date convertedDate = formatter.parse(date);

			Predicate<Product> predicate = new Predicate<Product>() {
				@Override
				public boolean apply(Product product) {
					return product.getIssueDate().before(convertedDate);
				}
			};

			Collection<Product> result = Collections2.filter(products, predicate);
			filteredProducts.addAll(result);

		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return filteredProducts;
	}

}