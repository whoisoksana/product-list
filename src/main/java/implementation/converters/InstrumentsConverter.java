package implementation.converters;

import java.util.ArrayList;
import java.util.List;

import implementation.MyException;
import implementation.hierarchy.Bond;
import implementation.hierarchy.Derivative;
import implementation.hierarchy.ETF;
import implementation.hierarchy.Fund;
import implementation.hierarchy.Future;
import implementation.hierarchy.Option;
import implementation.hierarchy.Product;
import implementation.hierarchy.Share;
import implementation.hierarchy.Warrant;
import implementation.json.ProductDTO;

public class InstrumentsConverter {

	public InstrumentsConverter() {
	}

	public Product createProduct(ProductDTO from) {
		Product product;
		switch (from.getProductType()) {
			case BOND:
				product = Bond.createProduct(from);
				break;
			case FUND:
				product = Fund.createProduct(from);
				break;
			case WARRANT:
				product = Warrant.createProduct(from);
				break;
			case ETF:
				product = ETF.createProduct(from);
				break;
			case DERIVATIVE:
				product = Derivative.createProduct(from);
				break;
			case SHARE:
				product = Share.createProduct(from);
				break;
			case OPTION:
				product = Option.createProduct(from);
				break;
			case FUTURES:
				product = Future.createProduct(from);
				break;
			default:
				try {
					throw new MyException("Product type is incorrect!");
				}
				catch (MyException e) {
					e.printStackTrace();
				}
				product = null;

		}

		return product;
	}

	public static String getUndSymbolForDerivatives(Product product) {
		String undSymbol;
		if (product instanceof Derivative) {
			undSymbol = ((Derivative) product).getUndSymbol();
		} else
			undSymbol = "";
		return undSymbol;
	}

	public static List<Derivative> getDerivatives(List<Product> products) throws Exception {
		List<Derivative> derivatives = new ArrayList<>();
		for (Product product : products) {
			if (product instanceof Derivative) {
				derivatives.add((Derivative) product);
			}
		}

		return derivatives;
	}

}