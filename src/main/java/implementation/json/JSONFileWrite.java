package implementation.json;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONFileWrite {

	public static void writeToJSONFile(List<ProductDTO> productsList) throws IOException {
		//FileWriter file = new FileWriter("Products_List.json");
		try (FileWriter file = new FileWriter("Products_List.json")) {

			Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
			String jsonText = gson.toJson(productsList);
			file.write(jsonText);
			//System.out.print(jsonText);

		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}
}
