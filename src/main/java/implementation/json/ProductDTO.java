package implementation.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

import implementation.attributes.ProductType;

public class ProductDTO {
	@SerializedName("Symbol")
	private String symbol;

	@SerializedName("Product Type")
	private ProductType productType;

	@SerializedName("Underlying symbol")
	private String undSymbol;

	@SerializedName("Stock exchange id")
	private Integer stockExchangeId;

	@SerializedName("Issue Date")
	private Date issueDate;

	@SerializedName("Maturity date")
	private Date maturityDate;

	public ProductDTO(String symbol, ProductType productType, String undSymbol, Integer stockExchangeId, Date issueDate, Date maturityDate) {
		this.symbol = symbol;
		this.productType = productType;
		this.undSymbol = undSymbol;
		this.stockExchangeId = stockExchangeId;
		this.issueDate = issueDate;
		this.maturityDate = maturityDate;
	}

	public ProductDTO() {
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getUndSymbol() {
		return undSymbol;
	}

	public void setUndSymbol(String undSymbol) {
		this.undSymbol = undSymbol;
	}

	public Integer getStockExchangeId() {
		return stockExchangeId;
	}

	public void setStockExchangeId(Integer stockExchangeId) {
		this.stockExchangeId = stockExchangeId;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder buffer = new StringBuilder();
		buffer.append("\n");
		buffer.append("Symbol: ");
		buffer.append(symbol);
		buffer.append(" Product Type: ");
		buffer.append(productType.toString());
		buffer.append(" Underlying symbol: ");
		buffer.append(undSymbol);
		buffer.append(" Stock exchange id: ");
		buffer.append(stockExchangeId);
		buffer.append(" Issue Date: ");
		buffer.append(df.format(issueDate));
		buffer.append(" Maturity date: ");
		buffer.append(df.format(maturityDate));

		return buffer.toString();
	}

}
