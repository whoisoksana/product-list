package implementation.json;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class JSONToJava<T> {

	private static Gson createGsonDateBuilder() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				try {
					return df.parse(json.getAsJsonPrimitive().getAsString());
				}
				catch (ParseException ex) {
					return null;
				}
			}
		});
		return gsonBuilder.create();
	}

	public T jsonToCollection(String jsonFileName) throws Exception {
		try {
			ParameterizedType type = (ParameterizedType) (getClass().getGenericSuperclass());
			Type genericType = type.getActualTypeArguments()[0];
			final BufferedReader br = new BufferedReader(new FileReader(jsonFileName));
			return createGsonDateBuilder().fromJson(br, genericType);
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
