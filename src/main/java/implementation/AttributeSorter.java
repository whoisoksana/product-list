package implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import implementation.hierarchy.Derivative;
import implementation.hierarchy.Product;
import implementation.converters.InstrumentsConverter;
import implementation.attributes.Attributes;
import implementation.attributes.ProductType;

public class AttributeSorter {

	private static List<Product> sortBySymbol(List<Product> products) {
		Collections.sort(products, new Comparator<Product>() {
			public int compare(final Product entry1, final Product entry2) {
				return entry1.getSymbol().compareTo(entry2.getSymbol());
			}
		});

		return products;
	}

	public static List<Product> sortByProductType(List<Product> products) {
		Collections.sort(products, new Comparator<Product>() {
			public int compare(final Product entry1, final Product entry2) {
				return entry1.getProductType().toString().compareTo(entry2.getProductType().toString());
			}
		});

		return products;
	}

	public static List<Product> sortByUndSymbol(List<Product> products) throws Exception {
		List<Derivative> derivatives = InstrumentsConverter.getDerivatives(products);
		Collections.sort(derivatives, new Comparator<Derivative>() {
			public int compare(final Derivative entry1, final Derivative entry2) {
				return entry1.getUndSymbol().compareTo(entry2.getUndSymbol());
			}
		});
		List<Product> sortedList = new ArrayList<>();
		sortedList.addAll(derivatives);

		return sortedList;
	}

	private static void sortByStockExchangeId(List<Product> products) {
		Collections.sort(products, new Comparator<Product>() {
			public int compare(final Product entry1, final Product entry2) {
				return entry1.getStockExchangeId().compareTo(entry2.getStockExchangeId());
			}
		});

	}

	public static List<Product> sort(List<Product> list, Attributes.ProductAttributes attributes) throws Exception {
		switch (attributes) {
			case SYMBOL:
				sortBySymbol(list);
				break;
			case PRODUCT_TYPE:
				sortByProductType(list);
				break;
			case UND_SYMBOL:
				sortByUndSymbol(list);
				break;
			case STOCK_EXCHANGE_ID:
				sortByStockExchangeId(list);
				break;
			case DATES:
				List<Product> sortedList = DateSorter.sortArray(DateSorter.initializeArray(list));
				list.clear();
				list.addAll(sortedList);
				break;
		}

		return list;
	}

	public static Integer countNumberOfProductsWithProductType(Set<Product> set, ProductType productType) {
		int counter = 0;
		for (Product temp : set) {
			if (productType.equals(temp.getProductType())) {
				counter++;
			}
		}
		return counter;
	}

	private static class DateSorter {

		public static Object[][] initializeArray(List<Product> products) {
			Object[][] dates = new Object[products.size()][3];
			for (int i = 0; i < products.size(); i++) {
				dates[i][0] = products.get(i).getIssueDate();
				dates[i][1] = products.get(i).getMaturityDate();
				dates[i][2] = products.get(i);
			}
			return dates;
		}

		public static List<Product> sortArray(Object[][] dates) {
			Arrays.sort(dates, new Comparator<Object[]>() {
				public int compare(Object[] entry1, Object[] entry2) {
					Date issueTime1 = (Date) entry1[0];
					Date issueTime2 = (Date) entry2[0];
					Date maturityTime1 = (Date) entry1[1];
					Date maturityTime2 = (Date) entry2[1];
					if (issueTime1.equals(issueTime2)) {
						return maturityTime1.compareTo(maturityTime2);
					} else {
						return issueTime1.compareTo(issueTime2);
					}
				}
			});

			List<Product> products = new LinkedList<Product>();
			for (Object[] date : dates) {
				products.add((Product) date[2]);

			}
			return products;
		}
	}
}
