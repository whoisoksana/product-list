package implementation;

import java.util.ArrayList;
import java.util.List;

import implementation.attributes.ProductsAttributesGenerator;
import implementation.db.ProductsDB;
import implementation.hierarchy.Fund;
import implementation.hierarchy.Product;
import implementation.json.JSONFileWrite;
import implementation.json.JSONToJava;
import implementation.json.ProductDTO;
import implementation.converters.InstrumentsConverter;

public class Main {
	public static void main(String[] args) throws Exception {
		final ProductsAttributesGenerator listOfProducts = ProductsAttributesGenerator.getInstance();

		JSONFileWrite.writeToJSONFile(listOfProducts.generateListOfProducts());

		List<ProductDTO> listDto = new JSONToJava<ArrayList<ProductDTO>>() {}.jsonToCollection("Products_List.json");
		List<Product> list = new ArrayList<>();

		for (ProductDTO productDTO : listDto) {
			InstrumentsConverter parser = new InstrumentsConverter();
			Product product = parser.createProduct(productDTO);
			list.add(product);
		}

		//		try (ProductsDB db = new ProductsDB()) {
		//			db.createTable();
		//			for (Product product : list) {
		//				System.out.println(product.toString());
		//				db.addProduct(product);
		//			}
		//
		//		}

		try (ProductsDB db = new ProductsDB()) {

			List<Fund> dblist = db.selectFundsFromDB();
			for (Fund bond : dblist) {
				System.out.println(bond.toString());
			}
		}
	}
}