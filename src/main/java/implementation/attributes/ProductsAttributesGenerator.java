package implementation.attributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import implementation.json.ProductDTO;

public final class ProductsAttributesGenerator {

	private static ProductsAttributesGenerator listOfProducts = new ProductsAttributesGenerator();
	private static ArrayList<Integer> range;

	/**
	 * generate shuffled array of int in range [1-999]
	 */
	static {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 1; i < 999; i++) {
			list.add(i);
		}

		Collections.shuffle(list);
		range = list;
	}

	private ProductsAttributesGenerator() {
	}

	public static ProductsAttributesGenerator getInstance() {
		return listOfProducts;
	}

	/**
	 * @return 2-5 random uppercase letters string
	 */
	private String generateSymbol() {
		int[] numberOfCharacters = {2, 3, 4, 5};
		return generateString(numberOfCharacters);
	}

	/**
	 * @return empty or 2-5 uppercase letters string
	 */
	private String generateUndSymbol() {
		int[] numberOfCharacters = {0, 2, 3, 4, 5};
		return generateString(numberOfCharacters);
	}

	/**
	 * @param numberOfCharacters array of possible length of output string
	 * @return string of random uppercase letters
	 */
	private String generateString(int[] numberOfCharacters) {
		String letters = "abcdefghijklmnopqrstuvwxyz";
		Random random = new Random();
		int index = random.nextInt(numberOfCharacters.length);
		StringBuilder rndString = new StringBuilder();

		for (int i = 0; i < numberOfCharacters[index]; i++) {
			rndString.append(letters.charAt(random.nextInt(letters.length())));
		}

		return rndString.toString().toUpperCase();
	}

	/**
	 * @return random number from range
	 */
	private Integer generateId() {
		Random random = new Random();
		int index = random.nextInt(range.size());

		return range.get(index);
	}

	/**
	 * @param startDate middle date of range of random
	 * @return random date in range 30 days from start date
	 */
	private Date generateRandomDate(DateTime startDate) {
		return startDate.plus(new Random().nextInt(30) * DateTimeConstants.MILLIS_PER_DAY).toDate();

	}

	/**
	 * @return random date in range of past 2 months
	 */
	private Date generateIssueDate() {
		DateTime startDate = new DateTime().withDayOfMonth(1).minusMonths(1);
		return generateRandomDate(startDate);
	}

	/**
	 * @return random date in range of next 2 months
	 */
	private Date generateMaturityDate() {
		DateTime startDate = new DateTime().withDayOfMonth(1).plusMonths(2);
		return generateRandomDate(startDate);
	}

	/**
	 * @return random list of 50 products with attributes: Symbol, Product Type, Underlying Symbol, Issue date, Maturity date
	 */
	public List<ProductDTO> generateListOfProducts() {
		LinkedList<ProductDTO> productList = new LinkedList<ProductDTO>();
		for (int i = 0; i < 50; i++) {
			ProductType productType = ProductTypeGenerator.randomProduct();
			ProductDTO dto = new ProductDTO();
			dto.setSymbol(generateSymbol());
			dto.setProductType(productType);
			if (productType.equals(ProductType.DERIVATIVE) || productType.equals(ProductType.WARRANT))
				dto.setUndSymbol(generateUndSymbol());
			dto.setStockExchangeId(generateId());
			dto.setIssueDate(generateIssueDate());
			dto.setMaturityDate(generateMaturityDate());
			productList.add(i, dto);
		}
		return productList;
	}
}
