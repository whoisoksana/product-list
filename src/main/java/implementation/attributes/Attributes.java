package implementation.attributes;

public class Attributes {
	public enum ProductAttributes {
		SYMBOL, PRODUCT_TYPE, UND_SYMBOL, STOCK_EXCHANGE_ID, DATES

	}
	private ProductAttributes attribute;

	public Attributes(ProductAttributes attribute) {
		this.attribute = attribute;
	}

	public ProductAttributes getAttribute() {
		return attribute;
	}
}