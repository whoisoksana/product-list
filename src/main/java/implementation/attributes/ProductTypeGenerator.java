package implementation.attributes;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ProductTypeGenerator {
	private static final List<ProductType> VALUES = Arrays.asList(ProductType.values());
	private static final int SIZE = VALUES.size();
	private static final Random random = new Random();

	/**
	 * @return random value of ProductType enum
	 */
	public static ProductType randomProduct() {
		return VALUES.get(random.nextInt(SIZE));
	}
}
