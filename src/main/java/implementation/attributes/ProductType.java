package implementation.attributes;

import com.google.gson.annotations.SerializedName;

public enum ProductType {
	@SerializedName("ETF")
	ETF("ETF"),

	@SerializedName("Share")
	SHARE("Share"),

	@SerializedName("Bond")
	BOND("Bond"),

	@SerializedName("Fund")
	FUND("Fund"),

	@SerializedName("Warrant")
	WARRANT("Warrant"),

	@SerializedName("Option")
	OPTION("Option"),

	@SerializedName("Futures")
	FUTURES("Futures"),

	@SerializedName("Derivative")
	DERIVATIVE("Derivative");

	private final String productType;

	private static final int size = ProductType.values().length;

	private ProductType(final String productType) {
		this.productType = productType;
	}

	@Override
	public String toString() {
		return productType;
	}

}
