package implementation.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import implementation.attributes.ProductType;
import implementation.converters.InstrumentsConverter;
import implementation.hierarchy.Bond;
import implementation.hierarchy.Derivative;
import implementation.hierarchy.ETF;
import implementation.hierarchy.Fund;
import implementation.hierarchy.Future;
import implementation.hierarchy.Option;
import implementation.hierarchy.Product;
import implementation.hierarchy.Share;
import implementation.hierarchy.Warrant;
import implementation.json.ProductDTO;

public class ProductsDB implements AutoCloseable {

	private Connection connection;

	public ProductsDB() {
		connection = ConnectionManager.getConnection();
	}

	public void createTable() {

		try (Statement stmt = connection.createStatement()) {

			DatabaseMetaData md = connection.getMetaData();
			ResultSet rs = md.getTables(null, null, "PRODUCTS", null);

			if (rs.next() && rs.getString(3).equals("PRODUCTS")) {
				System.out.println("Table already exist");
				rs.close();
			} else {
				String sql =
						"CREATE TABLE PRODUCTS " + "(ID  INTEGER PRIMARY KEY AUTOINCREMENT," + " SYMBOL   CHAR(5)    NOT NULL, "
								+ " PRODUCT_TYPE  CHAR(30) NOT NULL, " + " UNDERLYING_SYMBOL   CHAR(5) , "
								+ " STOCK_EXCHANGE_ID   INT   NOT NULL, " + "ISSUE_DATE	 DATE 	   NOT NULL, " + "MATURITY_DATE  DATE  NOT NULL)";
				stmt.executeUpdate(sql);
				rs.close();
				System.out.println("Table created successfully");
			}
		}

		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}

	public void addProduct(Product product) {

		Integer id = null;
		try (Statement stmt = connection.createStatement()) {
			connection.setAutoCommit(false);
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String isd = df.format(product.getIssueDate());
			String md = df.format(product.getMaturityDate());
			String undSymbol = InstrumentsConverter.getUndSymbolForDerivatives(product);
			String sql =
					"INSERT INTO PRODUCTS (ID,SYMBOL,PRODUCT_TYPE,UNDERLYING_SYMBOL,STOCK_EXCHANGE_ID, ISSUE_DATE, MATURITY_DATE) "
							+ "VALUES ( " + id + " , '" + product.getSymbol() + "' , '" + product.getProductType() + "' , '" + undSymbol
							+ "' , " + product.getStockExchangeId() + " , '" + isd + "' , '" + md + "' );";
			stmt.executeUpdate(sql);
			connection.commit();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Record created successfully");
	}

	public List<ProductDTO> selectAllProducts() {
		List<ProductDTO> products = new ArrayList();

		try (Statement stmt = connection.createStatement(); ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS;")) {

			while (resultSet.next()) {
				ProductDTO productDTO = new ProductDTO();

				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String symbol = resultSet.getString("SYMBOL");
				productDTO.setSymbol(symbol);

				String productType = resultSet.getString("PRODUCT_TYPE");
				productDTO.setProductType(ProductType.valueOf(productType.toUpperCase()));

				String undSymbol = resultSet.getString("UNDERLYING_SYMBOL");
				productDTO.setUndSymbol(undSymbol);

				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				productDTO.setStockExchangeId(stockExchangeId);

				String issueDate = resultSet.getString("ISSUE_DATE");
				productDTO.setIssueDate(df.parse(issueDate));

				String maturityDate = resultSet.getString("MATURITY_DATE");
				productDTO.setMaturityDate(df.parse(maturityDate));

				products.add(productDTO);
			}
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return products;
	}

	public List<Bond> selectBondsFromDB() {
		List<Bond> bonds = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Bond';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Bond bond = Bond.createProduct(symbol, stockExchangeId, issueDate, maturityDate);
				bonds.add(bond);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return bonds;
	}

	public List<Derivative> selectDerivativesFromDB() {
		List<Derivative> derivatives = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Derivative';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				String undSymbol = resultSet.getString("UNDERLYING_SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Derivative derivative = Derivative.createProduct(symbol, stockExchangeId, undSymbol, issueDate, maturityDate);

				derivatives.add(derivative);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return derivatives;
	}

	public List<Warrant> selectWarrantsFromDB() {
		List<Warrant> warrants = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Warrant';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				String undSymbol = resultSet.getString("UNDERLYING_SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Warrant warrant = Warrant.createProduct(symbol, stockExchangeId, undSymbol, issueDate, maturityDate);

				warrants.add(warrant);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return warrants;
	}

	public List<Option> selectOptionsFromDB() {
		List<Option> options = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Option';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				String undSymbol = resultSet.getString("UNDERLYING_SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Option option = Option.createProduct(symbol, stockExchangeId, undSymbol, issueDate, maturityDate);

				options.add(option);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return options;
	}

	public List<Future> selectFuturesFromDB() {
		List<Future> futures = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Futures';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				String undSymbol = resultSet.getString("UNDERLYING_SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Future future = Future.createProduct(symbol, stockExchangeId, undSymbol, issueDate, maturityDate);
				futures.add(future);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return futures;
	}

	public List<Share> selectSharesFromDB() {
		List<Share> shares = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Share';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Share share = Share.createProduct(symbol, stockExchangeId, issueDate, maturityDate);
				shares.add(share);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return shares;
	}

	public List<Fund> selectFundsFromDB() {
		List<Fund> funds = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='Fund';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				Fund fund = Fund.createProduct(symbol, stockExchangeId, issueDate, maturityDate);

				funds.add(fund);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return funds;
	}

	public List<ETF> selectETFsFromDB() {
		List<ETF> etfs = new ArrayList<>();
		try (Statement stmt = connection.createStatement();
				ResultSet resultSet = stmt.executeQuery("SELECT * FROM PRODUCTS WHERE PRODUCT_TYPE='ETF';")) {
			while (resultSet.next()) {
				String symbol = resultSet.getString("SYMBOL");
				Integer stockExchangeId = resultSet.getInt("STOCK_EXCHANGE_ID");
				String issueDate = resultSet.getString("ISSUE_DATE");
				String maturityDate = resultSet.getString("MATURITY_DATE");
				ETF etf = ETF.createProduct(symbol, stockExchangeId, issueDate, maturityDate);

				etfs.add(etf);
			}
		}
		catch (Exception e) {

			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return etfs;
	}

	@Override
	public void close() throws Exception {
		connection.close();
	}
}
