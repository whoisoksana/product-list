package implementation.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	private static String url = "jdbc:sqlite:test.db";
	private static String driverName = "org.sqlite.JDBC";
	private static Connection connection;


	public static Connection getConnection() {
		try {
			Class.forName(driverName);
			try {
				connection = DriverManager.getConnection(url);
			} catch (SQLException ex) {
				System.out.println("Failed to create the database connection.");
				System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
				System.exit(0);
			}
		} catch (ClassNotFoundException ex) {

			System.out.println("Driver not found.");
		}

		return connection;
	}
}