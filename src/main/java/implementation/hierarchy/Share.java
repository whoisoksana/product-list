package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Share extends Product implements NotTradable {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.SHARE;
	}

	public static Share createProduct(ProductDTO productDTO) {
		Share share = new Share();
		share.setSymbol(productDTO.getSymbol());
		share.setStockExchangeId(productDTO.getStockExchangeId());
		share.setIssueDate(productDTO.getIssueDate());
		share.setMaturityDate(productDTO.getMaturityDate());
		return share;
	}

	public static Share createProduct(String symbol, Integer stockExchangeId, String issueDate, String maturityDate) throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Share share = new Share();
		share.setSymbol(symbol);
		share.setStockExchangeId(stockExchangeId);
		share.setIssueDate(df.parse(issueDate));
		share.setMaturityDate(df.parse(maturityDate));
		return share;
	}
}
