package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import implementation.attributes.ProductType;

public abstract class Product {

	private String symbol;
	private Integer stockExchangeId;
	private Date issueDate;
	private Date maturityDate;

	protected ProductType productType;

	public Product() {
	}

	public String getSymbol() {
		return symbol;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Integer getStockExchangeId() {
		return stockExchangeId;
	}

	public void setStockExchangeId(Integer stockExchangeId) {
		this.stockExchangeId = stockExchangeId;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	@Override
	public String toString() {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String undSymbol;
		if (this instanceof Derivative) {
			undSymbol = ((Derivative) this).getUndSymbol();
		} else {
			undSymbol = "";
		}
		StringBuilder buffer = new StringBuilder();
		buffer.append("\n");
		buffer.append("Symbol: ");
		buffer.append(symbol);
		buffer.append(" Product Type: ");
		buffer.append(getProductType());
		buffer.append(" Underlying symbol: ");
		buffer.append(undSymbol);
		buffer.append(" Stock exchange id: ");
		buffer.append(stockExchangeId);
		buffer.append(" Issue Date: ");
		buffer.append(df.format(issueDate));
		buffer.append(" Maturity date: ");
		buffer.append(df.format(maturityDate));

		return buffer.toString();
	}
}
