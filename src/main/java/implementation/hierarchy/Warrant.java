package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Warrant extends Derivative {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.WARRANT;
	}

	public static Warrant createProduct(ProductDTO productDTO) {
		Warrant warrant = new Warrant();
		warrant.setSymbol(productDTO.getSymbol());
		warrant.setStockExchangeId(productDTO.getStockExchangeId());
		warrant.setUndSymbol(productDTO.getUndSymbol());
		warrant.setIssueDate(productDTO.getIssueDate());
		warrant.setMaturityDate(productDTO.getMaturityDate());
		return warrant;
	}

	public static Warrant createProduct(String symbol, Integer stockExchangeId, String undSymbol, String issueDate, String maturityDate)
			throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Warrant warrant = new Warrant();
		warrant.setSymbol(symbol);
		warrant.setStockExchangeId(stockExchangeId);
		warrant.setUndSymbol(undSymbol);
		warrant.setIssueDate(df.parse(issueDate));
		warrant.setMaturityDate(df.parse(maturityDate));
		return warrant;
	}

}
