package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Option extends Derivative {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.OPTION;
	}

	public static Option createProduct(ProductDTO productDTO) {
		Option option = new Option();
		option.setSymbol(productDTO.getSymbol());
		option.setStockExchangeId(productDTO.getStockExchangeId());
		option.setUndSymbol(productDTO.getUndSymbol());
		option.setIssueDate(productDTO.getIssueDate());
		option.setMaturityDate(productDTO.getMaturityDate());
		return option;
	}

	public static Option createProduct(String symbol, Integer stockExchangeId, String undSymbol, String issueDate, String maturityDate)
			throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Option option = new Option();
		option.setSymbol(symbol);
		option.setStockExchangeId(stockExchangeId);
		option.setUndSymbol(undSymbol);
		option.setIssueDate(df.parse(issueDate));
		option.setMaturityDate(df.parse(maturityDate));
		return option;
	}
}
