package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Fund extends Product implements NotTradable {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.FUND;
	}

	public static Fund createProduct(ProductDTO productDTO) {
		Fund fund = new Fund();
		fund.setSymbol(productDTO.getSymbol());
		fund.setStockExchangeId(productDTO.getStockExchangeId());
		fund.setIssueDate(productDTO.getIssueDate());
		fund.setMaturityDate(productDTO.getMaturityDate());
		return fund;
	}

	public static Fund createProduct(String symbol, Integer stockExchangeId, String issueDate, String maturityDate) throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Fund fund = new Fund();
		fund.setSymbol(symbol);
		fund.setStockExchangeId(stockExchangeId);
		fund.setIssueDate(df.parse(issueDate));
		fund.setMaturityDate(df.parse(maturityDate));
		return fund;
	}
}
