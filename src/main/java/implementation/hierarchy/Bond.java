package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Bond extends Product implements Tradable {

	@Override
	public ProductType getProductType() {
		return productType = ProductType.BOND;
	}

	public static Bond createProduct(ProductDTO productDTO) {
		Bond bond = new Bond();
		bond.setSymbol(productDTO.getSymbol());
		bond.setStockExchangeId(productDTO.getStockExchangeId());
		bond.setIssueDate(productDTO.getIssueDate());
		bond.setMaturityDate(productDTO.getMaturityDate());
		return bond;
	}

	public static Bond createProduct(String symbol, Integer stockExchangeId, String issueDate, String maturityDate) throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Bond bond = new Bond();
		bond.setSymbol(symbol);
		bond.setStockExchangeId(stockExchangeId);
		bond.setIssueDate(df.parse(issueDate));
		bond.setMaturityDate(df.parse(maturityDate));
		return bond;
	}

	@Override
	public void trade() {

	}
}
