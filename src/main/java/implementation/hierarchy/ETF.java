package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class ETF extends Product implements NotTradable {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.ETF;
	}

	public static ETF createProduct(ProductDTO productDTO) {
		ETF etf = new ETF();
		etf.setSymbol(productDTO.getSymbol());
		etf.setStockExchangeId(productDTO.getStockExchangeId());
		etf.setIssueDate(productDTO.getIssueDate());
		etf.setMaturityDate(productDTO.getMaturityDate());
		return etf;
	}

	public static ETF createProduct(String symbol, Integer stockExchangeId, String issueDate, String maturityDate) throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		ETF etf = new ETF();
		etf.setSymbol(symbol);
		etf.setStockExchangeId(stockExchangeId);
		etf.setIssueDate(df.parse(issueDate));
		etf.setMaturityDate(df.parse(maturityDate));
		return etf;
	}
}
