package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Future extends Derivative {
	@Override
	public ProductType getProductType() {
		return this.productType = ProductType.FUTURES;
	}

	public static Future createProduct(ProductDTO productDTO) {
		Future future = new Future();
		future.setSymbol(productDTO.getSymbol());
		future.setStockExchangeId(productDTO.getStockExchangeId());
		future.setUndSymbol(productDTO.getUndSymbol());
		future.setIssueDate(productDTO.getIssueDate());
		future.setMaturityDate(productDTO.getMaturityDate());
		return future;
	}

	public static Future createProduct(String symbol, Integer stockExchangeId, String undSymbol, String issueDate, String maturityDate)
			throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Future future = new Future();
		future.setSymbol(symbol);
		future.setStockExchangeId(stockExchangeId);
		future.setUndSymbol(undSymbol);
		future.setIssueDate(df.parse(issueDate));
		future.setMaturityDate(df.parse(maturityDate));
		return future;
	}
}
