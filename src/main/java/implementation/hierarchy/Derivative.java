package implementation.hierarchy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import implementation.attributes.ProductType;
import implementation.json.ProductDTO;

public class Derivative extends Product implements Tradable {

	private String undSymbol;

	public String getUndSymbol() {
		return undSymbol;
	}

	public void setUndSymbol(String undSymbol) {
		this.undSymbol = undSymbol;
	}

	@Override
	public ProductType getProductType() {
		return productType = ProductType.DERIVATIVE;
	}

	public static Derivative createProduct(ProductDTO productDTO) {
		Derivative derivative = new Derivative();
		derivative.setSymbol(productDTO.getSymbol());
		derivative.setStockExchangeId(productDTO.getStockExchangeId());
		derivative.setUndSymbol(productDTO.getUndSymbol());
		derivative.setIssueDate(productDTO.getIssueDate());
		derivative.setMaturityDate(productDTO.getMaturityDate());
		return derivative;
	}

	public static Derivative createProduct(String symbol, Integer stockExchangeId, String undSymbol, String issueDate, String maturityDate)
			throws Exception {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Derivative derivative = new Derivative();
		derivative.setSymbol(symbol);
		derivative.setStockExchangeId(stockExchangeId);
		derivative.setUndSymbol(undSymbol);
		derivative.setIssueDate(df.parse(issueDate));
		derivative.setMaturityDate(df.parse(maturityDate));
		return derivative;
	}

	@Override
	public void trade() {

	}
}
